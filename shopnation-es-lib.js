/**
 * Created by tovbin on 9/18/15.
 */

var util = require("util");
var events = require("events");

var searchDefaultOptions = {
    scroll: "60s"
};
/**
 * This class will do the search in ES and generate the following events:
 * - start
 * - hit
 * - error
 * - end
 * @param client
 * @param options
 * @constructor
 */
function Search(client, options) {
    this.client = client;
    this.total = undefined;
    this.options = options || searchDefaultOptions;
    this.paused = false;
    this.errorHandler = function (err) {
        this.emit("error", err);
    }.bind(this);
}
util.inherits(Search, events.EventEmitter);

Search.prototype.search = function (query) {
    query.scroll = query.scroll || this.options.scroll;
    this.options.scroll = query.scroll;
    this.client.search(query).then(processResults.bind(this), this.errorHandler);
};

function processResults(response) {
    var justStarted = false;
    if (this.total === undefined) {
        this.total = response.hits.total;
        this.emit("start", {total: this.total});
        justStarted = true;
    }
    this.hits = response.hits.hits;
    if (this.hits.length == 0 && !justStarted) {
        this.emit("end");
    } else {
        this.nextScroll = response._scroll_id;
        this.iterateBatch();
    }
}
Search.prototype.iterateBatch = function () {
    if (!this.paused) {
        if (this.hits.length > 0) {
            this.emit("hit", this.hits.shift());
            this.iterateBatch();
        } else {
            this.client.scroll({
                scrollId: this.nextScroll,
                scroll: this.options.scroll
            }).then(processResults.bind(this), this.errorHandler);
        }
    }
};
Search.prototype.pause = function () {
    this.paused = true;
};
Search.prototype.unpause = function () {
    if (this.paused) {
        this.paused = false;
        this.iterateBatch();
    }
};

var defaultBatchSize = 5000;

function UpdateByQuery(client, batchSize) {
    this.client = client;
    this.batchSize = batchSize || defaultBatchSize;
    this.errorHandler = function (err) {
        this.emit("error", err);
    }.bind(this);
    this.batch = [];
    this.search = new Search(client, {scroll : searchDefaultOptions.scroll, size : this.batchSize});
    this.updated = 0;
    this.errors = 0;
}
util.inherits(UpdateByQuery, events.EventEmitter);

UpdateByQuery.prototype.update = function (query, updater) {
    var isFunc = typeof (updater) == "function";
    this.search.on("start", function(evt){
        this.emit("start", evt);
    }.bind(this));
    this.search.on("end", function(evt){
        this.sendBatch(function(){
            this.emit("end", evt);
        }.bind(this));
    }.bind(this));
    this.search.on("error", function(evt){this.emit("error", evt)}.bind(this));
    this.search.on("hit", function(hit){
        var result = isFunc ? updater(hit._source) : updater;
        if (result) {
            this.batch.push({
                update:{
                    _index : hit._index,
                    _type : hit._type,
                    _id : hit._id,
                    _retry_on_conflict: 3
                }
            },{
                doc : result
            });
        }
        if (this.batch.length >= this.batchSize * 2) {
            this.search.pause();
            this.sendBatch(function(){
                this.search.unpause();
            }.bind(this));
        }
    }.bind(this));
    if (!isFunc) {
        query.fields = [];
    }
    this.search.search(query);
};
UpdateByQuery.prototype.sendBatch = function(callback) {
    if (this.batch.length) {
        var currBatch = this.batch;
        this.batch = [];
        this.client.bulk({body: currBatch}).then(function(response) {
            response.items.forEach(function(item){
                if (item.update.status == 200) {
                    this.updated++;
                } else {
                    this.errors++;
                    console.error(item.update.error);
                }
            }.bind(this));
            this.emit("progress", {updated : this.updated, errors:this.errors});
            if (callback) {
                callback();
            }
        }.bind(this), this.errorHandler);
    } else if (callback) {
        callback();
    }
};

module.exports = {
    Search: Search,
    UpdateByQuery : UpdateByQuery
};